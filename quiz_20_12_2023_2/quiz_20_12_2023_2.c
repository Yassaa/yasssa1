#include <stdio.h>
#include "myheader.h"

int main()
{
    system("clear");

    char nama[50];
    char gol = '';
    char status[10];

    float gapok = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();
    input_data(nama, &gol, status);
    hitung_gaji(gol, status, &gapok, &tunja, &prosen_pot);
    pot = hitung_potongan(gapok, tunja, prosen_pot);
    float gaber = hitung_gaji_bersih(gapok, tunja, pot);
    output_data(gapok, tunja, pot, gaber);

    return 0;
}
